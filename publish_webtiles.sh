#!/usr/bin/env bash

PUBDIR=/share/projects/sixp/pub/wtrgb

mkdir -p $PUBDIR
cp -vr web_tiles/rgb/*/* $PUBDIR

for file in web_tiles/rgb/*/openlayers.html; do 
    cp -v ${file} $PUBDIR/$(basename ${file%/*})-${file//*\/}
done
