# SixP - WP3

Data and code repository for the [Positive Plant-Plant interactions and
spatial Patterns in Pyrenean Post-mine tailings (SixP) project][sixp].

[sixp]: https://sixp.inria.fr

## Setup

There are two setup stages proposed:

1. Base only: with Git. For Code, lightweight data, QGIS project and
   results. You can stick to this one if you don't need to pull all the
   lossless data,
2. Full setup: with Git and DVC. To get the complete data to reproduce
   or make new results (~30 Go of data).

### Base only with QGIS project

With [git], clone this repo with these commands:

```
git clone https://gitlab.com/fguiotte/sixp.git
cd sixp
```

The QGIS files are in the `qgis` directory. The files are saved with
QGIS version 3.24.2-Tisler (this release contains a dedicated patch 
for the very high resolution images of SixP).

[git]: https://git-scm.com/download

### Full setup (with all data)

This repository use [DVC](https://dvc.org/) to track the `data`
directories.

First, install DVC (e.g. `pip install dvc`). Then, you can init this
repo with these commands: 

```
git clone https://gitlab.com/fguiotte/sixp.git
cd sixp
dvc pull
```

#### Advanced setup (ssh)

To modify the data or pull with your [cluster-irisa ssh account][wiki],
you need to setup locally your user-name the first time. Use this
command replacing "fguiotte" with your cluster-irisa username:

```
dvc remote modify --local cluster user fguiotte 
```

Then you can pull with ssh `dvc pull -r cluster` or push with `dvc push
-r cluster`.

To set ssh the default remote, you can setup `dvc remote default --local cluster`
then directly use `dvc pull` and `dvc push`.

#### Local setup (on cluster-irisa)

You can use the local dvc remote `local-cluster` if you work on `dmis`
or `volta1` with `dvc pull -r local-cluster` or `dvc push -r
local-cluster`.

[wiki]: https://cluster-irisa.univ-ubs.fr/wiki/acces-ssh
