#!/usr/bin/env bash

AJ_DIRECTORY=/data/x/chantiers/6P
IRISA_DIRECTORY=./data
SITE="$1"
CAPTOR="$2"

search_irisa(){
    echo Searching IRISA directories:
    find $IRISA_DIRECTORY -maxdepth 1 -type d
}
search_vis(){
    echo Searching visible dataset:
    find $AJ_DIRECTORY -name '*_vis.7z'
}

search_vnir(){
    echo Searching ir dataset:
    find $AJ_DIRECTORY -name '*_4canaux.zip.001' -or -name '*_4canaux.7z'
}

ask_site(){
    echo 'Which dataset would you like to import? The name should be IRISA like Chichoue_Milieu_Haut.'
    echo -n '> '
    read
    SITE="$REPLY"
}

ask_captor(){
    echo 'Which captor would you like to import? [rgb/vnir]'
    echo -n '> '
    read
    CAPTOR="$REPLY"
}

set_irisa_dir(){
    IRISA_DIR=${IRISA_DIRECTORY}/${SITE}/ortho_${CAPTOR}
}

set_aj_file_vnir(){
    AJ_NAME=${SITE//_/}
    AJ_FILE=$(find $AJ_DIRECTORY -name ${AJ_NAME}_4canaux.7z -or -name ${AJ_NAME}_4canaux.zip.001)
    echo $AJ_FILE
}
    
set_aj_file_vis(){
    AJ_NAME=${SITE//_/}
    AJ_FILE=$(find $AJ_DIRECTORY -name ${AJ_NAME}_vis.7z)
    echo $AJ_FILE
}

unzip_multi(){
    AJ_FILE_GLOB=${AJ_FILE//.*}
    AJ_FILES="$(ls $AJ_FILE_GLOB.zip.???)"
    mkdir -p $IRISA_DIR
    echo Merging $AJ_FILES
    cat $AJ_FILE_GLOB.zip.??? > $IRISA_DIR/all.zip
    unzip $IRISA_DIR/all.zip -d $IRISA_DIR
    rm -v $IRISA_DIR/all.zip
}
    
unzip_7z(){
    7z -o${IRISA_DIR} e ${AJ_FILE}
}

unzip(){
    if test ${AJ_FILE//*.} == '7z'; then
        unzip_7z
    else
        unzip_multi
    fi
}

dvc_add(){
    dvc add $IRISA_DIR
}

main(){
    if test $# -ne 2; then
        search_irisa
        search_vis
        search_vnir
        ask_site
        ask_captor
    fi

    set_irisa_dir

    if test $CAPTOR == 'rgb'; then
        set_aj_file_vis
    elif test $CAPTOR == 'vnir'; then
        set_aj_file_vnir
    fi
    unzip
    dvc_add
}

main $*
