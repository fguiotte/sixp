for site in $(find data -name *.vrt); do
    echo "  $(dirname $site | sed -e 's/data\///' ) $(basename $site) OVR:"
    echo "    cmd: gdaladdo -r average --config COMPRESS_OVERVIEW ZSTD -ro $site"
    echo "    deps:"
    echo "    - $site"
    echo "    outs:"
    echo "    - $site.ovr"
done
