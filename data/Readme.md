## Biomasse

Pour obtenir la position des mesures de biomasse, utiliser
`biomasse_initiale.csv` et faire une jointure avec `points_sixp.csv` sur
`'Jeton Quadrat'` (+strip letter and punctuation) et `'NUM_POINT'` pour
`'LETTRE_POINT' == 'c'`.
